/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mavenproject1;

/**
 *
 * @author pattarapon
 */
import java.util.Scanner;
public class labXO2 {
    static Scanner kb = new Scanner(System.in);
    static char player = 'X';
    static int row,col;
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static boolean InputContinue() {
        System.out.print("Please input continue or exit : ");
        String go = kb.next();
        if (go.equals("continue")) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    table[i][j] = '-';
                }
            }
            player = 'O';
            return false;
        }
     return true;
    }
    static boolean IsWin(){
        if(CheckWinRow()){
            return true;
        }if(CheckWinCol()){
            return true;
        }return CheckWinDia();
        
    }
    static boolean CheckWinRow(){
        for(int i=0;i<3;i++){
            if(table[row-1][i]!=player){
                return false;
            }
        }
        return true;
    }
    static boolean CheckWinCol(){
        for(int i=0;i<3;i++){
            if(table[i][col-1]!=player){
                return false;
            }
        }
        return true;
    }
    static boolean CheckWinDia(){
         for(int i=0,j=2;i<3;i++,j--){
             if(table[i][i]!=player&&table[i][j]!=player){
                 return false;
             }
         }
         
        return true;
    }
    static boolean IsDraw(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j]=='-'){
                    return false;
                }
            }
            
        }
        return true;
    }
    static void ShowWin(){
        printTable();
        System.out.println(player+" win");
    }
    static void InputRowCol(){
        System.out.print("Please input row,col : ");
        row = kb.nextInt();
        col = kb.nextInt();
        if(table[row-1][col-1]=='-'){
            table[row-1][col-1]=player;
            
        }else{
            System.out.println("cann't move");
            InputRowCol();
        }
    }
    static void printTurn(){
        System.out.println(player+" turn");
    }
    static void ChangePlayer(){
        if(player=='X'){
            player='O';
        }else{
            player='X';
        }
    }
    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    static void printWelcome(){
        System.out.println("Welcome to XO");
    }
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            InputRowCol();
            if (IsWin()){
                ShowWin();
                if(InputContinue()){
                    break;
                }
            }
            if(IsDraw()){
                printTable();
                System.out.println("Tie on one win");
                if(InputContinue()){
                    break;
                }
            }
            ChangePlayer();
        }
    }
}
